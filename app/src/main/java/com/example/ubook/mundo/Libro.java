package com.example.ubook.mundo;

import java.io.Serializable;

public class Libro implements Serializable {
    public static final String NAME_COLLECTION = "libros";

    private int codigo;
    private String titulo;
    private String autor;
    private int paginas;
    private int precioVenta;
    private int precioCompra;
    private int descuento;

    public Libro() {

    }

    public Libro(int codigo, String titulo, String autor) {
        this.codigo = codigo;
        this.titulo = titulo;
        this.autor = autor;
    }

    public Libro(int codigo, String titulo, String autor, int paginas, int precioVenta, int precioCompra, int descuento)
    {
        this.codigo = codigo;
        this.titulo = titulo;
        this.autor = autor;
        this.paginas = paginas;
        this.precioVenta = precioVenta;
        this.precioCompra = precioCompra;
        this.descuento = descuento;
    }

    public int getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(int precioVenta) {
        this.precioVenta = precioVenta;
    }

    public int getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(int precioCompra) {
        this.precioCompra = precioCompra;
    }

    public int getGanancia() {
        if(descuento > 0)
        {
            int descuento = (int) (precioVenta - ((precioVenta * getDescuento()) / 100)) -  precioCompra;
            return descuento;
        }

        return (getPrecioVenta() - getPrecioCompra());
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String nombre) {
        this.titulo = nombre;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getPaginas() {
        return paginas;
    }

    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }

    @Override
    public String toString() {
        return "Libro{" +
                "codigo=" + codigo +
                ", titulo='" + titulo + '\'' +
                ", autor='" + autor + '\'' +
                ", paginas='" + paginas + '\'' +
                ", descuento=" + descuento + '\'' +
                ", precio de compra=" + precioCompra + '\'' +
                ", precio de venta=" + getPrecioVenta() + '\'' +
                '}';
    }
}
