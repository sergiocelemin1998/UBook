package com.example.ubook.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ubook.R;
import com.example.ubook.mundo.Libro;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class LibroAdapter extends FirestoreRecyclerAdapter<Libro, LibroAdapter.LibroHolder>
{
    private int ganancia;
    public LibroAdapter(@NonNull FirestoreRecyclerOptions<Libro> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull LibroHolder holder, int position, @NonNull Libro libro) {
        holder.txtTitulo.setText(libro.getTitulo());
        holder.txtAutor.setText(libro.getAutor());
        holder.txtPaginas.setText("Páginas: " + libro.getPaginas());
        holder.txtCodigo.setText("Código: " + libro.getCodigo());
        holder.txtPrecioVenta.setText("Precio de venta: " + libro.getPrecioVenta());

        if(libro.getDescuento() > 0)
        {
            int descuento = (int) libro.getPrecioVenta() - ((libro.getPrecioVenta() * libro.getDescuento()) / 100);
            holder.txtPrecioDescuento.setText("Con descuento: " + descuento);
            holder.txtDescuento.setText("Descuento: " + libro.getDescuento() + "%");
        }
        else
        {
            holder.txtDescuento.setText("0%");
            holder.txtPrecioDescuento.setText("Sin descuento");
        }
    }

    @NonNull
    @Override
    public LibroHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_libro,
                viewGroup, false);
        return new LibroHolder(v);
    }

    public class LibroHolder extends RecyclerView.ViewHolder
    {
        private TextView txtTitulo;
        private TextView txtAutor;
        private TextView txtPaginas;
        private TextView txtPrecioVenta;
        private TextView txtPrecioDescuento;
        private TextView txtDescuento;
        private TextView txtCodigo;

        public LibroHolder(@NonNull final View itemView) {
            super(itemView);
            txtTitulo = itemView.findViewById(R.id.txtTitulo);
            txtAutor = itemView.findViewById(R.id.txtAutor);
            txtPaginas = itemView.findViewById(R.id.txtPaginas);
            txtPrecioVenta = itemView.findViewById(R.id.txtPrecioVenta);
            txtPrecioDescuento = itemView.findViewById(R.id.txtPrecioDescuento);
            txtDescuento = itemView.findViewById(R.id.txtDescuento);
            txtCodigo = itemView.findViewById(R.id.txtCodigo);
        }
    }
}
