package com.example.ubook.servicio;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.ubook.activities.ActivityRegistro;
import com.example.ubook.activities.MainActivity;
import com.example.ubook.mundo.Libro;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.File;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.ArrayList;


public class ServicioUBook implements Serializable
{
    private File archivo;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    CollectionReference librosRef = db.collection(Libro.NAME_COLLECTION);

    public ServicioUBook(String ruta)
    {
        archivo = new File(ruta);
    }

    public void agregarLibro(Libro libro)
    {
        if(buscarLibro(libro.getCodigo()) == null)
        {

        }
    }

    public Libro buscarLibro(int pCodigo)
    {
        final Libro[] libro = {null};
        DocumentReference docRef = librosRef.document(String.valueOf(pCodigo));
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                libro[0] = documentSnapshot.toObject(Libro.class);
                Log.d("Asd", libro[0].toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });


        return libro[0];
    }

    public boolean eliminarLibro(int pCodigo)
    {
        //TODO: ELIMINAR REGISTRO EN LA DB.
       return false;
    }

    //TODO: HACER METODO.

    public void listarLibros()
    {

    }

    //todo: actualizar libro
    public void actualizarLibro(Libro libro)
    {



    }

}
