package com.example.ubook.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ubook.R;
import com.example.ubook.mundo.Libro;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class ActivityRegistro extends AppCompatActivity
{
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    CollectionReference librosRef = db.collection(Libro.NAME_COLLECTION);

    private TextView txtCodigo, txtTitulo, txtAutor, txtPaginas, txtPrecioCompra, txtPrecioVenta, txtDescuento;
    private Button btnAgregar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        txtCodigo = (TextView)findViewById(R.id.txtCodigo);
        txtAutor =(TextView)findViewById(R.id.txtAutor);
        txtTitulo = (TextView)findViewById(R.id.txtTitulo);
        txtPaginas = (TextView)findViewById(R.id.txtPaginas);
        txtPrecioCompra = (TextView)findViewById(R.id.txtPrecioCompra);
        txtPrecioVenta = (TextView)findViewById(R.id.txtPrecioVenta);
        txtDescuento = (TextView)findViewById(R.id.txtDescuento);
        btnAgregar = (Button)findViewById(R.id.btnAgregar);
    }

    public void agregarLibro(View view)
    {
        if(validarCampos())
        {
            Toast.makeText(this, "Campos invalidos", Toast.LENGTH_SHORT).show();
        }
        else
        {
            int codigo = Integer.parseInt(txtCodigo.getText().toString());

            DocumentReference docRef = librosRef.document(String.valueOf(codigo));
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful())
                    {
                        DocumentSnapshot document = task.getResult();
                        if (!document.exists())
                        {
                            agregarLibro();
                        }
                        else
                        {
                            Toast.makeText(ActivityRegistro.this,
                                    "Ya existe un libro con ese código",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(ActivityRegistro.this,
                                "Error: " + task.getException().getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void agregarLibro()
    {
        int codigo = Integer.parseInt(txtCodigo.getText().toString());
        String autor = txtAutor.getText().toString();
        String titulo =  txtTitulo.getText().toString();
        int paginas = Integer.parseInt(txtPaginas.getText().toString());
        int precioVenta = Integer.parseInt(txtPrecioVenta.getText().toString());
        int precioCompra = Integer.parseInt(txtPrecioCompra.getText().toString());
        int descuento = Integer.parseInt(txtDescuento.getText().toString());

        Libro libro = new Libro(codigo, autor, titulo, paginas, precioVenta, precioCompra, descuento);

        librosRef.document(String.valueOf(libro.getCodigo()))
                .set(libro)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(ActivityRegistro.this,
                                "Se ha registrado el libro exitosamente",
                                Toast.LENGTH_SHORT).show();
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ActivityRegistro.this,
                                "No se ha registrado el libro: " + e.getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private boolean validarCampos()
    {
        String codigo = txtCodigo.getText().toString();
        String titulo =  txtTitulo.getText().toString();
        String autor = txtAutor.getText().toString();
        String paginas = txtPaginas.getText().toString();
        String precioVenta = txtPrecioVenta.getText().toString();
        String precioCompra = txtPrecioCompra.getText().toString();
        String descuento = txtDescuento.getText().toString();

        if(codigo.trim().equals("") || titulo.trim().equals("") ||  autor.trim().equals("") ||
                paginas.trim().equals("") ||  precioVenta.trim().equals("") || precioCompra.trim().equals("") ||
                precioCompra.trim().equals("") || descuento.trim().equals(""))
        {
            return true;
        }
        else if(!isNumeric(codigo) || !isNumeric(paginas) || !isNumeric(precioVenta) ||
                !isNumeric(precioCompra) || !isNumeric(descuento))
        {
            return true;
        }

        return false;
    }

    public static boolean isNumeric(String cadena)
    {
        boolean resultado;

        try
        {
            Integer.parseInt(cadena);
            resultado = true;
        }
        catch (NumberFormatException excepcion)
        {
            resultado = false;
        }

        return resultado;
    }
}
