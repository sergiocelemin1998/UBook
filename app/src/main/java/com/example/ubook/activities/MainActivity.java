package com.example.ubook.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.ubook.R;
import com.example.ubook.servicio.ServicioUBook;

public class MainActivity extends AppCompatActivity
{
    public static final int REQUEST_CODE_REGISTRAR_LIBRO = 0;

    private Button btnAgregar, btnEliminar, btnBuscar, btnListar;
    private ServicioUBook servicioUBook;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAgregar = findViewById(R.id.btnAgregar);
        btnBuscar = findViewById(R.id.btnBuscar);
        btnEliminar = findViewById(R.id.btnEliminar);
        btnListar = findViewById(R.id.btnListar);

        String ruta = getApplicationContext().getFilesDir().getPath() + "/" + getString(R.string.nombreArchivo);
        servicioUBook = new ServicioUBook(ruta);
    }

    public void btnAgregar(View view)
    {
        Intent intent = new Intent(this, ActivityRegistro.class);
        startActivity(intent);
    }

    public void btnBuscar(View view)
    {
        Intent intent = new Intent(this, ActivityInformacion.class);
        startActivity(intent);
    }

    public void btnEliminar(View view)
    {
        Intent intent = new Intent(this, ActivityEliminacion.class);
        startActivity(intent);
    }

    public void btnListar(View view)
    {
        Intent intent = new Intent(this, ActivityLibros.class);
        startActivity(intent);
    }
}