package com.example.ubook.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ubook.R;
import com.example.ubook.mundo.Libro;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class ActivityEliminacion extends AppCompatActivity
{
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    CollectionReference librosRef = db.collection(Libro.NAME_COLLECTION);

    private TextView txtCodigo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eliminacion);
        txtCodigo = findViewById(R.id.txtCodigo);
    }

    public void btnEliminar(View view)
    {
        if(txtCodigo.getText().toString().equals(""))
        {
            Toast.makeText(this, "El campo del código está vacio.", Toast.LENGTH_SHORT).show();
        }
        else
        {
            int codigo = Integer.parseInt(txtCodigo.getText().toString());

            DocumentReference docRef = librosRef.document(String.valueOf(codigo));
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful())
                    {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists())
                        {
                            eliminarLibro();
                        }
                        else
                        {
                            Toast.makeText(ActivityEliminacion.this,
                                    "No existe ningún libro con ese libro",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(ActivityEliminacion.this,
                                "Error: " + task.getException().getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void eliminarLibro()
    {
        int codigo = Integer.parseInt(txtCodigo.getText().toString());

        librosRef.document(String.valueOf(codigo))
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid)
                    {
                        Toast.makeText(ActivityEliminacion.this,
                                "Se ha elimado el libro.",
                                Toast.LENGTH_SHORT).show();
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e)
                    {
                        Toast.makeText(ActivityEliminacion.this,
                                "No se ha encontrado el libro: " + e.getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void eliminarLibro(int codigo)
    {
        librosRef.document(String.valueOf(codigo))
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid)
                    {
                        Toast.makeText(ActivityEliminacion.this,
                                "Se ha elimado el libro.",
                                Toast.LENGTH_SHORT).show();
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e)
                    {
                        Toast.makeText(ActivityEliminacion.this,
                                "No se ha encontrado el libro: " + e.getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void btnEliminarTodos(View view)
    {
        librosRef
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful() && task.getResult().size() > 0)
                        {
                            for (DocumentSnapshot document : task.getResult())
                            {
                                int codigo = Integer.parseInt(document.get("codigo").toString());
                                eliminarLibro(codigo);
                            }
                        }
                        else
                        {
                            Toast.makeText(ActivityEliminacion.this, "No existen libros.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
