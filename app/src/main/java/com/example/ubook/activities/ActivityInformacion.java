package com.example.ubook.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ubook.R;
import com.example.ubook.mundo.Libro;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class ActivityInformacion extends AppCompatActivity
{
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    CollectionReference librosRef = db.collection(Libro.NAME_COLLECTION);

    private TextView txtCodigo, txtTitulo, txtAutor, txtPaginas, txtPrecioCompra, txtPrecioVenta, txtDescuento;
    private Button btnBuscar, btnActualizar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacion);

        txtCodigo = findViewById(R.id.txtCodigo);
        txtTitulo = findViewById(R.id.txtTitulo);
        txtAutor = findViewById(R.id.txtAutor);
        txtPaginas = findViewById(R.id.txtPaginas);
        txtPrecioCompra = findViewById(R.id.txtPrecioCompra);
        txtPrecioVenta = findViewById(R.id.txtPrecioVenta);
        txtDescuento = findViewById(R.id.txtDescuento);
        btnBuscar = findViewById(R.id.btnBuscar);
        btnActualizar = findViewById(R.id.btnActualizar);
    }

    public void btnBuscar(View view)
    {
        String codigo = txtCodigo.getText().toString().trim();
        if(codigo.equals("") || !isNumeric(codigo))
        {
            Toast.makeText(this, "El campo del código es inválido.", Toast.LENGTH_SHORT).show();
        }
        else
        {
            DocumentReference docRef = librosRef.document(codigo);
            docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    Libro libro = documentSnapshot.toObject(Libro.class);
                    if(libro != null)
                    {
                        txtTitulo.setText(libro.getTitulo());
                        txtAutor.setText(libro.getAutor());
                        txtPaginas.setText(libro.getPaginas() + "");
                        txtPrecioCompra.setText(libro.getPrecioCompra()+"");
                        txtPrecioVenta.setText(libro.getPrecioVenta()+"");
                        txtDescuento.setText(libro.getDescuento()+"");
                    }
                    else
                    {
                        Toast.makeText(ActivityInformacion.this,
                                "No existe ningún libro con ese libro",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }


    public void btnActualizar(View view)
    {
        if(validarCampos())
        {
            Toast.makeText(this, "Campos invalidos.", Toast.LENGTH_SHORT).show();
        }
        else
        {
            int codigo = Integer.parseInt(txtCodigo.getText().toString());

            DocumentReference docRef = librosRef.document(String.valueOf(codigo));
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful())
                    {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists())
                        {
                            actualizarLibro();
                        }
                        else
                        {
                            Toast.makeText(ActivityInformacion.this,
                                    "No existe ningún libro con ese código.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(ActivityInformacion.this,
                                "Error: " + task.getException().getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void actualizarLibro()
    {
        int codigo = Integer.parseInt(txtCodigo.getText().toString());
        String titulo =  txtTitulo.getText().toString();
        String autor = txtAutor.getText().toString();
        int paginas = Integer.parseInt(txtPaginas.getText().toString());
        int precioVenta = Integer.parseInt(txtPrecioVenta.getText().toString());
        int precioCompra = Integer.parseInt(txtPrecioCompra.getText().toString());
        int descuento = Integer.parseInt(txtDescuento.getText().toString());

        Libro libro = new Libro(codigo, titulo, autor, paginas, precioVenta, precioCompra, descuento);

        librosRef.document(String.valueOf(libro.getCodigo()))
                .set(libro)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        limpiarCajasTexto();
                        Toast.makeText(ActivityInformacion.this,
                                "Se ha actualizado el libro exitosamente.",
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ActivityInformacion.this,
                                "No se ha actualizado el libro: " + e.getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private boolean validarCampos()
    {
        String codigo = txtCodigo.getText().toString();
        String titulo =  txtTitulo.getText().toString();
        String autor = txtAutor.getText().toString();
        String paginas = txtPaginas.getText().toString();
        String precioVenta = txtPrecioVenta.getText().toString();
        String precioCompra = txtPrecioCompra.getText().toString();
        String descuento = txtDescuento.getText().toString();

        if(codigo.trim().equals("") || titulo.trim().equals("") ||  autor.trim().equals("") ||
                paginas.trim().equals("") ||  precioVenta.trim().equals("") || precioCompra.trim().equals("") ||
                precioCompra.trim().equals("") || descuento.trim().equals(""))
        {
            return true;
        }
        else if(!isNumeric(codigo) || !isNumeric(paginas) || !isNumeric(precioVenta) ||
                !isNumeric(precioCompra) || !isNumeric(descuento))
        {
            return true;
        }

        return false;
    }

    public static boolean isNumeric(String cadena)
    {
        boolean resultado;

        try
        {
            Integer.parseInt(cadena);
            resultado = true;
        }
        catch (NumberFormatException excepcion)
        {
            resultado = false;
        }

        return resultado;
    }

    private void limpiarCajasTexto()
    {
        txtCodigo.setText("");
        txtTitulo.setText("");
        txtAutor.setText("");
        txtPaginas.setText("");
        txtPrecioCompra.setText("");
        txtPrecioVenta.setText("");
        txtDescuento.setText("");
    }
}
