package com.example.ubook.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ubook.R;
import com.example.ubook.adapters.LibroAdapter;
import com.example.ubook.mundo.Libro;
import com.firebase.ui.common.ChangeEventType;
import com.firebase.ui.firestore.ChangeEventListener;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.firebase.ui.firestore.ObservableSnapshotArray;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import javax.annotation.Nullable;

public class ActivityLibros extends AppCompatActivity
{
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    CollectionReference librosRef = db.collection(Libro.NAME_COLLECTION);

    private TextView txtGanancia;
    private RecyclerView rvLibros;
    private RadioGroup rgFiltros;
    private RadioButton rbBaratos, rbCortos, rbLiquidacion;

    private LibroAdapter librosAdapter;
    private LibroAdapter libroAdapterBaratos;
    private LibroAdapter libroAdapterCortos;
    private LibroAdapter libroAdapterLiquidacion;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_libros);

        inicializarTexto();
        inicializarBotones();
        inicializarRvLibros();
        inicializarAdapters();
        eventoRgFiltros();
        listarLibrosBaratos();
    }

    private void inicializarTexto()
    {
        txtGanancia = findViewById(R.id.txtGanancia);
    }

    private void inicializarBotones()
    {
        rgFiltros = findViewById(R.id.radioFiltros);
        rbBaratos = findViewById(R.id.radioBaratos);
        rbCortos = findViewById(R.id.radioCortos);
        rbLiquidacion = findViewById(R.id.radioDescuentos);
    }

    private void inicializarRvLibros()
    {
        rvLibros = findViewById(R.id.rvLibros);
        rvLibros.setLayoutManager(new LinearLayoutManager(this));
    }

    private void inicializarAdapters()
    {
        inicializarLibrosAdapters();
        inicializarLibrosBaratosAdapter();
        inicializarLibrosCortosAdapter();
        inicializarLibrosLiquidacionAdapter();
    }

    private void inicializarLibrosAdapters()
    {
        Query query = FirebaseFirestore
                .getInstance()
                .collection(Libro.NAME_COLLECTION);
        FirestoreRecyclerOptions<Libro> options= new FirestoreRecyclerOptions.Builder<Libro>()
                .setQuery(query, Libro.class)
                .build();
        librosAdapter = new LibroAdapter(options);
        actualizarGanancia(librosAdapter);
    }

    private void inicializarLibrosBaratosAdapter()
    {
        Query query = FirebaseFirestore
                .getInstance()
                .collection(Libro.NAME_COLLECTION)
                .whereLessThan("precioVenta", 100000);
        FirestoreRecyclerOptions<Libro> options= new FirestoreRecyclerOptions.Builder<Libro>()
                .setQuery(query, Libro.class)
                .build();
        libroAdapterBaratos = new LibroAdapter(options);
    }

    private void inicializarLibrosCortosAdapter()
    {
        Query query = FirebaseFirestore
                .getInstance()
                .collection(Libro.NAME_COLLECTION)
                .whereLessThan("paginas", 300);
        FirestoreRecyclerOptions<Libro> options= new FirestoreRecyclerOptions.Builder<Libro>()
                .setQuery(query, Libro.class)
                .build();
        libroAdapterCortos = new LibroAdapter(options);
    }

    private void inicializarLibrosLiquidacionAdapter()
    {
        Query query = FirebaseFirestore
                .getInstance()
                .collection(Libro.NAME_COLLECTION)
                .whereGreaterThan("descuento", 20);
        FirestoreRecyclerOptions<Libro> options= new FirestoreRecyclerOptions.Builder<Libro>()
                .setQuery(query, Libro.class)
                .build();
        libroAdapterLiquidacion = new LibroAdapter(options);
    }

    private void eventoGanancia(Query query)
    {
        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e)
            {
                int ganancia = 0;
                for (QueryDocumentSnapshot document : queryDocumentSnapshots)
                {
                    Toast.makeText(ActivityLibros.this, document.getData().toString(), Toast.LENGTH_SHORT).show();
                    ganancia += Integer.parseInt(document.get("ganancia").toString());
                }
                Toast.makeText(ActivityLibros.this, ganancia + "", Toast.LENGTH_SHORT).show();
                txtGanancia.setText("Ganancia: " + ganancia);
            }
        });
    }

    private void eventoRgFiltros()
    {
        rgFiltros.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(rbBaratos.getId() == checkedId)
                {
                    libroAdapterCortos.stopListening();
                    libroAdapterLiquidacion.stopListening();
                    libroAdapterBaratos.startListening();
                    listarLibrosBaratos();
                }
                else if(rbCortos.getId() == checkedId)
                {
                    libroAdapterBaratos.stopListening();
                    libroAdapterLiquidacion.stopListening();
                    libroAdapterCortos.startListening();
                    listarLibrosCortos();
                }
                else
                {
                    libroAdapterBaratos.stopListening();
                    libroAdapterCortos.stopListening();
                    libroAdapterLiquidacion.startListening();
                    listarLibrosLiquidacion();
                }
            }
        });
    }

    private void listarLibrosBaratos()
    {
        rvLibros.setAdapter(libroAdapterBaratos);
    }

    private void listarLibrosCortos()
    {
        rvLibros.setAdapter(libroAdapterCortos);
    }

    private void listarLibrosLiquidacion()
    {
        rvLibros.setAdapter(libroAdapterLiquidacion);
    }

    private void actualizarGanancia(LibroAdapter libroAdapter)
    {
        final ObservableSnapshotArray<Libro> snapshotArray = libroAdapter.getSnapshots();
        snapshotArray.addChangeEventListener(new ChangeEventListener() {
            @Override
            public void onChildChanged(@NonNull ChangeEventType type, @NonNull DocumentSnapshot snapshot, int newIndex, int oldIndex) {

            }

            @Override
            public void onDataChanged() {
                int ganancia = 0;
                for(int i = 0; i < snapshotArray.size(); i++)
                {
                    Libro libroActual = snapshotArray.get(i);
                    ganancia += libroActual.getGanancia();
                }

                txtGanancia.setText("Ganancia: " + ganancia);
            }

            @Override
            public void onError(@NonNull FirebaseFirestoreException e) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        librosAdapter.startListening();
        libroAdapterBaratos.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        librosAdapter.stopListening();
        libroAdapterBaratos.stopListening();
        libroAdapterCortos.stopListening();
        libroAdapterLiquidacion.stopListening();
    }
}
